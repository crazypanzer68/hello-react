class Calculator {
    plus(x, y) {
        return x + y
    }
    minus(x, y) {
        return x - y
    }
    multiple(x, y) {
        return x * y
    }
    divide(x, y) {
        if (y === 0) {
            return 'Divide by zero'
        } else {
            return x / y
        }
    }
}
module.exports = Calculator