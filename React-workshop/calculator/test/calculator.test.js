var assert = require('assert');
const Calculator = require('../calculator')

describe('Test Calculate', function () {
    const calculator = new Calculator()
    it('5+5 should return 10', function () {
        assert.equal(calculator.plus(5, 5), 10);
    });
    it('5-5 should return 10', function () {
        assert.equal(calculator.minus(5, 5), 0);
    });
    it('5*5 should return 25', function () {
        assert.equal(calculator.multiple(5, 5), 25);
    });
    it('5/5 should return 1', function () {
        assert.equal(calculator.divide(5, 5), 1);
    });
    it('5/0 should return Divide by zero', function () {
        assert.equal(calculator.divide(5, 0), 'Divide by zero');
    });
});